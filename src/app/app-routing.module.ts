import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {HomePageComponent} from "./home-page/home-page.component";
import {RoutingErrorComponent} from "./routing-error/routing-error.component";
import {ReminderListComponent} from "./reminder-list/reminder-list.component";
import {LogoutComponent} from "./logout/logout.component";
import {RouteGuardService} from "./services/route-guard.service";
import {ReminderComponent} from "./reminder/reminder.component";

const routes: Routes = [
  {path: '', component: HomePageComponent, canActivate:[RouteGuardService]},
  {path: 'login', component: LoginComponent},
  {path: 'home/:username', component: HomePageComponent, canActivate: [RouteGuardService]},
  {path: 'reminders', component: ReminderListComponent, canActivate: [RouteGuardService]},
  {path: 'reminders/:id', component: ReminderComponent, canActivate: [RouteGuardService]},
  {path: 'logout', component: LogoutComponent, canActivate: [RouteGuardService]},
  {path: '**', component: RoutingErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
