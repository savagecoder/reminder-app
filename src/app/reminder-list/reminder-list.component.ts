import { Component, OnInit } from '@angular/core';
import {RemindersService} from "../services/data/reminders.service";
import {Router} from "@angular/router";

export class Reminder {
  constructor(
    public id: number,
    public description: string,
    public done: boolean,
    public targetDate: Date
  ) {
  }
}

@Component({
  selector: 'app-reminder-list',
  templateUrl: './reminder-list.component.html',
  styleUrls: ['./reminder-list.component.css']
})
export class ReminderListComponent implements OnInit {

  reminders: Reminder[] | undefined;
  message: string | undefined;

  // reminders = [
  //   new Reminder(1, 'Learn Fullstack', false, new Date()),
  //   new Reminder(2, 'Visit Tahiti', false, new Date()),
  //   new Reminder(3, 'Learn Fullstack', false, new Date()),
  // ];

  constructor(
    private remindersService: RemindersService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.refreshReminders();
  }

  deleteReminder(id: number) {
    this.remindersService.deleteReminder('Jay', id).subscribe(
      data => {
        this.message = 'Delete Successful';
        this.refreshReminders();
      }
    );
  }

  editReminder(id: number) {
    this.router.navigate(['reminders', id])
  }

  refreshReminders() {
    this.remindersService.retrieveAllReminders('jay').subscribe(
      data => {
        this.reminders = data;
      }
    )
  }

  createReminder() {
    this.router.navigate(['reminders', -1]);
  }

}
