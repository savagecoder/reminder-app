import { Component, OnInit } from '@angular/core';
import {RemindersService} from "../services/data/reminders.service";
import {Reminder} from "../reminder-list/reminder-list.component";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'app-reminder',
  templateUrl: './reminder.component.html',
  styleUrls: ['./reminder.component.css'],
})
export class ReminderComponent implements OnInit {

  id: number = +this.route.snapshot.params['id'];
  reminder: Reminder;

  reminderForm = this.fb.group({
    id: [this.id],
    description: ['', [Validators.required]],
    targetDate: ['', [Validators.required]],
    done: [false]
  })

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private remindersService: RemindersService,
    private fb: FormBuilder,
  ) {}

  ngOnInit(): void {
    if (this.id !== -1) {
      this.remindersService.retrieveReminder('Jay', this.id).subscribe(
        data => {
          this.reminder = data;
          this.reminderForm.controls['description'].setValue(this.reminder.description);
          if (this.reminder.targetDate) {
            this.reminderForm.controls['targetDate'].setValue(new Date(this.reminder.targetDate).toLocaleDateString());
          }
        }
      );
    }

  }

  saveReminder() {
    if (!this.reminderForm.invalid) {
      if (this.id !== -1 ) {
        this.remindersService.createReminder('Jay', this.reminderForm.value).subscribe(data => {
          console.log(data);
          this.router.navigate(['reminders'])
        })
      }
      this.remindersService.updateReminder('Jay', this.id, this.reminderForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate(['reminders'])
      });
    }
  }

  get targetDate() {
    return this.reminderForm.controls['targetDate'];
  }

  get description() {
    return this.reminderForm.controls['description'];
  }
}
