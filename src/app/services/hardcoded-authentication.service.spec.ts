import { TestBed } from '@angular/core/testing';

import { HardcodedAuthenticationService } from './harcoded-authentication.service';

describe('HarcodedAuthenticationService', () => {
  let service: HardcodedAuthenticationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HardcodedAuthenticationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
