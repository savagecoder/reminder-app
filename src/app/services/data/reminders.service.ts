import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Reminder} from "../../reminder-list/reminder-list.component";

@Injectable({
  providedIn: 'root'
})
export class RemindersService {

  constructor(
    private http: HttpClient
  ) { }

  retrieveAllReminders(username: string) {
    return this.http.get<Reminder[]>(`http://localhost:8080/users/${username}/reminders`);
  }

  deleteReminder(username: string, id: number) {
    return this.http.delete(`http://localhost:8080/users/${username}/reminders/${id}`)
  }

  retrieveReminder(username: string, id: number) {
    return this.http.get<Reminder>(`http://localhost:8080/users/${username}/reminders/${id}`)
  }

  updateReminder(username: string, id: number, reminder: Reminder) {
    return this.http.put<Reminder>(`http://localhost:8080/users/${username}/reminders/${id}`, reminder);
  }

  createReminder(username: string, reminder: Reminder) {
    return this.http.post<Reminder>(`http://localhost:8080/users/${username}/reminders`, reminder);
  }
}
