import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

export interface HelloWorldBean {
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class HomeDataService {

  constructor(
    private http: HttpClient
  ) { }

  executeHelloWorldBeanService() {
    return this.http.get<HelloWorldBean>('http://localhost:8080/hello-world-bean');
  }

  executeHelloWorldBeanServiceWithPath(name: string) {
    let basicAuthHeaderString = this.createBasicAuthHttpHeaders();

    let headers = new HttpHeaders({
      Authorization: basicAuthHeaderString
    })
    return this.http.get<HelloWorldBean>(`http://localhost:8080/hello-world/${name}`, {
      headers
    });
  }

  createBasicAuthHttpHeaders() {
    let username = 'Jay';
    let password = 'dummy';
    let basicAuthHeaderString = 'Basic ' + window.btoa(username + ':' + password);
    return basicAuthHeaderString;
  }
}
