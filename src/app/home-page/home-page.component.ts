import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HelloWorldBean, HomeDataService} from "../services/data/home-data.service";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  message = 'Some Welcome Message';
  name = '';
  welcomeMessage: string | undefined;

  constructor(
    private route: ActivatedRoute,
    private homeDataService: HomeDataService
  ) { }

  ngOnInit(): void {
    this.name = this.route.snapshot.params['username']
  }

  getWelcomeMessage() {
    this.homeDataService.executeHelloWorldBeanService()
      .subscribe(response => {
        this.handleSuccessResponse(response);
      }, error => {
        this.handleErrorResponse(error);
      });
  }

  getWelcomeMessageWithParam() {
    this.homeDataService.executeHelloWorldBeanServiceWithPath(this.name)
      .subscribe(response => {
        this.handleSuccessResponse(response);
      }, error => {
        this.handleErrorResponse(error);
      });
  }

  handleSuccessResponse(response: HelloWorldBean) {
    this.welcomeMessage = response.message
  }

  handleErrorResponse(error: any) {
    this.welcomeMessage = error.error.message;
  }

}
